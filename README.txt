
Site Administrator Module that monitors releases of installed contributed
projects (currently only modules, but planned to also support themes).

To quickly determine your contrib availability for the next major release (5.0),
install on the current release (4.7) and look for messages about upgrade
availability.  Then use the links to either the CVS repositories or the Drupal
project pages.

TODO: 
 * email notification option

Installation
------------

Copy the module to your module directory and then enable on the admin
modules page.  View the release status page at admin/releases.

Author
------
Doug Green
douggreen@douggreenconsulting.com
